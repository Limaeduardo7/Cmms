// pages/dashboard.js
import Layout from '../components/Layout';

export default function Dashboard() {
    return (
        <Layout title="Dashboard - Sistema de Gestão CMMS">
            <h1>Dashboard</h1>
            <p>Visão geral do sistema e métricas importantes.</p>
        </Layout>
    );
}
